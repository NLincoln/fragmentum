export default Klass => (...args) => new Klass(...args);
